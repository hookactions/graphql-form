# GraphQL Form

Easily add a GraphQL mutation to any HTML form without a framework like React or Vue.

## Usage

```html
<html>
  <head>
      <script type="text/javascript" src="graphql-form.js"></script>
  </head>
  <body>
    <form method="post" action="https://api.example.com/v1/graphql"
        data-gql="mutation CreateUser($email: String!) {createUser(email: $email)}">
        <input type="email" name="email" placeholder="Email (e.g. elijah@example.com)"> <button
        type="submit">Submit</button>
    </form>
    <script type="text/javascript">
        (function () {
            const elements = document.querySelectorAll("form[data-gql]");
            const formEl = elements[0];
            const form = new GraphQLForm(formEl, function(xhr) {
                console.log(JSON.parse(xhr.responseText), xhr.status);
            });
        }())
    </script>
  </body>
</html>
```

## Why?

We're using [Hugo](https://gohugo.io) to have a simple website, but we want to have our form post to our GraphQL backend.

## Browser support

Most if not all modern browsers. The library only uses `XMLHTTPRequest` and old style js classes via functions.
